import Foundation
import RealmSwift

protocol DataBaseServiceProtocol {
    associatedtype Item
    func save(object: Item)
    func get() -> [Item]
}

class DataBaseService<T: Object> {
    
    lazy var realm: Realm = {
        let config = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 1) {
                }
            })
        Realm.Configuration.defaultConfiguration = config
        return try! Realm(configuration: config)
    }()
    
    func save(object: T) {
        try? realm.write( {
            realm.add(object)
        })
    }
    
    func get() -> [T] {
        let students = realm.objects(T.self)
        return Array(students)
    }
 
}
