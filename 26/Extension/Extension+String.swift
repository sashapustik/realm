import Foundation

extension String {
    var isValidEmail: Bool {
        let emailPred = NSPredicate(
            format: Constants.Format.predicateFormat,
            Constants.Format.mail
        )
        return emailPred.evaluate(with: self)
    }
    
    var isValidString: Bool {
        let stringPred = NSPredicate(
            format: Constants.Format.predicateFormat,
            Constants.Format.onlyString
        )
        return stringPred.evaluate(with: self)
    }
}
