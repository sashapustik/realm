import UIKit

extension CASpringAnimation {
    static func pump(view: UIView) {
        let animation = CASpringAnimation(keyPath: Constants.Animation.key)
        animation.fromValue = Constants.Animation.fromValue
        animation.toValue = Constants.Animation.toValue
        animation.duration = Constants.Animation.duration
        view.layer.add(
            animation,
            forKey: Constants.Animation.key
        )
    }
}
