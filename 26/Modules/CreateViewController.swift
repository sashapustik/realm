import UIKit
import RealmSwift

class CreateViewController: UIViewController {
    
    private var type: Object.Type
    
    private let tableView = UITableView()
    
    private var delegate: UITableViewDelegate
    private var dataSource: UITableViewDataSource
    
    var object: Object?
    
    weak var sendDataDelegate: SendDataDelegate?
    //MARK: Init view and provider
    init(
        textFieldModels: [TextFieldModel],
        object: Object?,
        type: Object.Type
    ) {
        let createProvider = CreateProvider(
            textFieldModels: textFieldModels,
            object: object,
            type: type
        )
        self.type = type
        self.object = createProvider.getObject()
        dataSource = createProvider
        delegate = createProvider
        super.init(nibName: nil, bundle: nil)
        createProvider.createDelegate = self
        createProvider.setInfoDelegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = delegate
        tableView.dataSource = dataSource
        view.backgroundColor = .white
        hiddenKeyboard(to: tableView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        guard let delegate = delegate as? CreateProvider else { return }
        delegate.reloadData()
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
        initTableView()
        setTableViewConstraints()
        setAddStudent()
    }
    
    //MARK: Init tableView
    private func initTableView() {
        tableView.separatorStyle = .none
        tableView.keyboardDismissMode = .onDrag
        tableViewRegisterCells()
        self.view.addSubview(tableView)
    }
    
    //MARK: Register Cells
    private func tableViewRegisterCells() {
        tableView.register(
            CreateTableViewCell.self,
            forCellReuseIdentifier: Constants.CellId.createCell
        )
        tableView.register(
            UITableViewCell.self,
            forCellReuseIdentifier: Constants.CellId.defaultCell
        )
        tableView.register(
            ItemTableViewCell.self,
            forCellReuseIdentifier: Constants.CellId.itemCell
        )
        tableView.register(
            HeaderView.self,
            forHeaderFooterViewReuseIdentifier: Constants.CellId.headerId
        )
    }
    
    //MARK: Table view constraints
    private func setTableViewConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        let constraint = Constants.Constraints.self
        let constraints = [
            tableView.topAnchor.constraint(
                equalTo: self.view.topAnchor,
                constant: .zero
            ),
            tableView.trailingAnchor.constraint(
                equalTo: self.view.trailingAnchor,
                constant: -constraint.editEdge
            ),
            tableView.leadingAnchor.constraint(
                equalTo: self.view.leadingAnchor,
                constant: constraint.editEdge
            ),
            tableView.bottomAnchor.constraint(
                equalTo: self.view.bottomAnchor,
                constant: .zero
            )
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    //MARK: Navigation button
    private func setAddButton() {
        let addButton =
            UIBarButtonItem(
                barButtonSystemItem: .add,
                target: self,
                action: #selector(add)
            )
        if let course = object as? CourseModel {
            navigationItem.rightBarButtonItems = [
                addButton,
                .init(
                    title: course.teacher != nil
                        ? Constants.Titles.changeTeacher
                        : Constants.Titles.addTeacher,
                    style: .done,
                    target: self,
                    action: #selector(addTeacher)
                )
            ]
        } else {
            navigationItem.rightBarButtonItem = addButton
        }
    }
    
    //MARK: Add button for course model
    private func setAddStudent() {
        let button = AddButton()
        
        button.addTarget(
            self,
            action: #selector(addStudents),
            for: .touchUpInside
        )
        let constraints = [
            button.bottomAnchor.constraint(
                equalTo: tableView.bottomAnchor,
                constant: -Constants.ButtonSetup.edgeXY
            ),
            button.trailingAnchor.constraint(
                equalTo: tableView.trailingAnchor,
                constant: -Constants.ButtonSetup.edgeXY
            ),
            button.widthAnchor.constraint(greaterThanOrEqualToConstant: Constants.ButtonSetup.size),
            button.heightAnchor.constraint(equalToConstant:  Constants.ButtonSetup.size)
        ]
        
        if object is CourseModel {
            guard let object = object as? CourseModel else { return }
            let strings = Set(object.getAllData())
            if strings.count != 1 {
                view.addSubview(button)
                NSLayoutConstraint.activate(constraints)
            }
        }
    }

    //MARK: Handlers
    @objc
    private func add() {
        guard
            let realm = try? Realm(),
            let object = object
        else { return }
        let indexPath = IndexPath(
            item: .zero,
            section: Constants.Section.editSection
        )
        guard
            let editView = tableView.cellForRow(at: indexPath) as? CreateTableViewCell
        else { return }
        sendDataDelegate = editView
        sendDataDelegate?.sendData().forEach {
            let (key, value) = $0
            try? realm.write {
                object.setValuesForKeys([key: value])
            }
        }
        
        try? realm.write {
            realm.add(object)
        }
        navigationController?.popViewController(animated: true)
    }

    @objc
    private func addTeacher() {
        guard let course = object as? CourseModel else { return }
        let teacherProvider = TeacherProvider()
        let viewController = ViewController(
            delegate: teacherProvider,
            dataSource: teacherProvider
        )
        teacherProvider.setCourse(course: course)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    @objc
    private func addStudents() {
        let studentProvider = StudentProvider()
        let viewController = ViewController(
            delegate: studentProvider,
            dataSource: studentProvider
        )
        guard let course = object as? CourseModel else { return }
        studentProvider.setCourse(course: course)
        setInfo(to: viewController)
    }
}

//MARK: - CreateDelegate
extension CreateViewController: CreateDelegate {
    func addButton(isHidden: Bool) {
        isHidden
            ? navigationItem.rightBarButtonItems = []
            : setAddButton()
    }
    
    func setErrorAlert(message: String) {
        self.alert(message: message)
    }
    
    func setInfo(with object: Object) -> [String] {
        switch type {
        case is StudentModel.Type:
            guard let student = object as? StudentModel else { return [] }
            return student.getAllData()
        case is TeacherModel.Type:
            guard let teacher = object as? TeacherModel else { return [] }
            return teacher.getAllData()
        case is CourseModel.Type:
            guard let course = object as? CourseModel else { return [] }
            return course.getAllData()
        default:
            return []
        }
    }
}

//MARK: - SetInfoDelegate
extension CreateViewController: SetInfoDelegate {
    func setInfo(to viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
}
