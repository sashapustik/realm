import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    
    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)
        window?.rootViewController = setTabBarControllerSetup()
        window?.makeKeyAndVisible()
    }

    private func setTabBarControllerSetup() -> UITabBarController {
        let tabBarController = UITabBarController()
        tabBarController.delegate = self
        tabBarController.setViewControllers(
            setupViewControllers(),
            animated: true
        )
        setupTabBar(in: tabBarController)
        
        return tabBarController
    }
    
    private func setupTabBar(in tabBarController: UITabBarController) {
        let images = [
            UIImage(systemName: Constants.TabBarSetup.Images.student),
            UIImage(systemName: Constants.TabBarSetup.Images.course),
            UIImage(systemName: Constants.TabBarSetup.Images.teacher)
        ]
        let names = [
            Constants.TabBarSetup.Names.student,
            Constants.TabBarSetup.Names.course,
            Constants.TabBarSetup.Names.teacher
        ]
        guard let tabBarItems = tabBarController.tabBar.items else { return }
        for index in  .zero..<tabBarItems.count {
            tabBarItems[index].image = images[index]
            tabBarItems[index].title = names[index]
        }
        tabBarController.tabBar.tintColor = #colorLiteral(red: 0.9966171384, green: 0.5380721688, blue: 0.4036274552, alpha: 1)
    }
    
    private func setupViewControllers() -> [UINavigationController] {
        //Providers
        let studentProvider = StudentProvider()
        let courseProvider = CourseProvider()
        let teacherProvider = TeacherProvider()
        
        //Student view controler and her provider
        let studentViewController = ViewController(
            delegate: studentProvider,
            dataSource: studentProvider
        )
        studentProvider.sendDelegate = studentViewController
        
        //Course view controler and her provider
        let courseViewController = ViewController(
            delegate: courseProvider,
            dataSource: courseProvider
        )
        courseProvider.sendDelegate = courseViewController
        
        //Teacher view controler and her provider
        let teacherViewController = ViewController(
            delegate: teacherProvider,
            dataSource: teacherProvider
        )
        teacherProvider.sendDelegate = teacherViewController
        
        return [
            UINavigationController(rootViewController: studentViewController),
            UINavigationController(rootViewController: courseViewController),
            UINavigationController(rootViewController: teacherViewController)
        ]
    }
}

extension SceneDelegate: UITabBarControllerDelegate  {
    func tabBarController(
        _ tabBarController: UITabBarController,
        shouldSelect viewController: UIViewController
    ) -> Bool {

        guard
            let fromView = tabBarController.selectedViewController?.view,
            let toView = viewController.view
        else { return false }

        if fromView != toView {
            UIView.transition(
                from: fromView,
                to: toView,
                duration: Constants.TabBarSetup.anomationDuration,
                options: [.transitionCrossDissolve],
                completion: nil
            )
        }

        return true
    }
}
