import Foundation

enum TextFieldId: String {
    case name
    case surname
    case subject
    case department
    case mailAddress
}
