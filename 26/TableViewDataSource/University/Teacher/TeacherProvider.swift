import UIKit
import RealmSwift

class TeacherProvider: NSObject, UniversityProtocol {
    typealias Item = TeacherModel
    
    var items: [TeacherModel]
    private var course: CourseModel?
    
    private let databaseService = DataBaseService<TeacherModel>()
    weak var sendDelegate: SetInfoDelegate?
    
    override init() {
        self.items = databaseService.get()
    }
    
    func reloadData() {
        self.items = databaseService.get()
    }
    
    func setCourse(course: CourseModel) {
        self.course = course
    }
    
    func courseIsSelected() -> Bool {
        return course != nil
    }
}

//MARK: UITableViewDataSource
extension TeacherProvider: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: Constants.CellId.defaultCell,
            for: indexPath
        ) as? ItemTableViewCell
        else { return UITableViewCell() }
        course?.teacher == items[indexPath.row]
            ? cell.setBackgroundColor(#colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), lineColor: #colorLiteral(red: 0.8416554928, green: 0.8417772651, blue: 0.8451326489, alpha: 1))
            : cell.setBackgroundColor(#colorLiteral(red: 0.8416554928, green: 0.8417772651, blue: 0.8451326489, alpha: 1), lineColor: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
        let fullName = "\(items[indexPath.row].name ) \(items[indexPath.row].surname)"
        let subjectCount = "\(Constants.Titles.courseCountTitle) \(items[indexPath.row].courses.count)"
        cell.setConfig(name: fullName, data: subjectCount)
        
        return cell
    }
}

//MARK: UITableViewDelegate
extension TeacherProvider: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard course == nil else {
            guard let course = course else { return }
            guard course.teacher == items[indexPath.row] else {
                guard let indexCourse = course.teacher?.courses.firstIndex(of: course) else {
                    try? databaseService.realm.write {
                        course.teacher = items[indexPath.row]
                        course.teacher?.courses.append(course)
                    }
                    tableView.reloadData()
                return
                }
                try? databaseService.realm.write {
                    if course.teacher != nil {
                        course.teacher?.courses.remove(at: indexCourse)
                    }
                    course.teacher = items[indexPath.row]
                    course.teacher?.courses.append(course)
                }
                return
            }
            guard let indexCourse = course.teacher?.courses.firstIndex(of: course) else { return }
            try? databaseService.realm.write {
                course.teacher?.courses.remove(at: indexCourse)
                course.teacher = nil
            }
            tableView.reloadData()
            return
        }
        let tags = Constants.TabBarSetup.Tags.self
        let viewController = CreateViewController(
            textFieldModels: TextFieldModel.getTextFields(with: tags.teacher.rawValue),
            object:items[indexPath.row],
            type: TeacherModel.self)
        sendDelegate?.setInfo(to: viewController)
    }
    
    func tableView(
        _ tableView: UITableView,
        editingStyleForRowAt indexPath: IndexPath
    ) -> UITableViewCell.EditingStyle {
        guard course == nil else { return .none }
        return .delete
    }
    
    func tableView(
        _ tableView: UITableView,
        commit editingStyle: UITableViewCell.EditingStyle,
        forRowAt indexPath: IndexPath
    ) {
        guard editingStyle == .delete else { return }
        try? databaseService.realm.write {
            databaseService.realm.delete(items[indexPath.row])
            items.remove(at: indexPath.row)
        }
        tableView.reloadData()
    }
}
