import Foundation
import RealmSwift

protocol CreateDelegate: class {
    var object: Object? { get set }
    func setInfo(with object: Object) -> [String]
    func setErrorAlert(message: String)
    func addButton(isHidden: Bool)
}
