import UIKit

protocol SetInfoDelegate: class {
    func setInfo(to viewController: UIViewController)
}
