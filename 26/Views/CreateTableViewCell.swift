import UIKit
import RealmSwift

protocol SendDataDelegate: NSObject {
    func sendData() -> ([(String, String)])
}

class CreateTableViewCell: UITableViewCell {

    weak var delegate: CreateDelegate?
    private var type: Object.Type?
    
    private var textFieldsModels = [TextFieldModel]()
    private var textFields = [UITextField]()
    private var stackView = UIStackView()
    
    //MARK: Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        setClearSelectedView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Set constraints for stack view
    private func setConstraints(to stackView: UIStackView) {
        let constraint = Constants.Constraints.self
        let constraints = [
            stackView.topAnchor.constraint(
                equalTo: self.topAnchor,
                constant: constraint.top
            ),
            stackView.trailingAnchor.constraint(
                equalTo: self.trailingAnchor,
                constant: constraint.trailing
            ),
            stackView.leadingAnchor.constraint(
                equalTo: self.leadingAnchor,
                constant: constraint.leading
            ),
            stackView.bottomAnchor.constraint(
                equalTo: self.bottomAnchor,
                constant: -constraint.top
            )
        ]
        
         NSLayoutConstraint.activate(constraints)
    }
    
    private func setClearSelectedView() {
        let view = UIView()
        view.backgroundColor = .white
        self.selectedBackgroundView = view
    }
    
    //MARK: Set configuration data
    func setConfig(
        textFieldsModels: [TextFieldModel],
        to type: Object.Type,
        data: [String]?
    ) {
        stackView.removeFromSuperview()
        self.type = type
        self.textFieldsModels = textFieldsModels
        stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = Constants.Constraints.editEdge
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = .init(
            top: Constants.InfoCellSetup.margin,
            left: Constants.InfoCellSetup.margin,
            bottom: Constants.InfoCellSetup.margin,
            right: Constants.InfoCellSetup.margin
        )
        stackView.backgroundColor = #colorLiteral(red: 0.9898464084, green: 0.5342234373, blue: 0.3995503187, alpha: 1)
        stackView.layer.cornerRadius = Constants.cellCorner
        addSubview(stackView)
        
        guard let objects = data else { return }
        print(objects)
        for index in textFieldsModels.indices {
            let textField = UITextField()
            let infoCell = Constants.InfoCellSetup.self
            textField.tag = index
            textField.backgroundColor = UIColor.white.withAlphaComponent(infoCell.textFieldColorAlpha)
            textField.text = objects[index]
            textField.translatesAutoresizingMaskIntoConstraints = false
            textField.keyboardType = textFieldsModels[index].textFieldKeyboardStyle
            textField.placeholder = textFieldsModels[index].placeholder
            textField.delegate = self
            textField.borderStyle = .roundedRect
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textFields.append(textField)
            stackView.addArrangedSubview(textField)
        }
        if objects.count != textFieldsModels.count {
            let label = UILabel()
            label.text = Constants.Titles.teacher
            label.textColor = .white
            label.font = .systemFont(ofSize: Constants.Titles.teacherLabelFontSize)
            stackView.addArrangedSubview(label)
            for index in textFieldsModels.count..<objects.count {
                
                let label = UILabel()
                label.text = objects[index]
                stackView.addArrangedSubview(label)
            }
        }
        setConstraints(to: stackView)
    }
}

//MARK: - UITextFieldDelegate
extension CreateTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard textField.tag == textFieldsModels.count - 1 else {
            guard
                let views = stackView.subviews[textField.tag + 1] as? UITextField
            else { return false }
            views.becomeFirstResponder()
            return true
        }
        textField.endEditing(true)
        return true
    }

    func textFieldDidChangeSelection(_ textField: UITextField) {
        textFields.forEach {
            guard let text = $0.text else { return }
            switch textFieldsModels[$0.tag].textFieldId {
            case .mailAddress:
                guard text.isValidEmail else {
                    DispatchQueue.main.async {
                        self.delegate?.addButton(isHidden: true)
                    }
                    $0.borderColor(.red)
                    return
                }
                
                $0.borderColor(.clear)
                DispatchQueue.main.async {
                    self.delegate?.addButton(isHidden: false)
                }
            default:
                guard text.isValidString else {
                    DispatchQueue.main.async {
                        self.delegate?.addButton(isHidden: true)
                    }
                    $0.borderColor(.red)
                    return
                }
                
                DispatchQueue.main.async {
                    self.delegate?.addButton(isHidden: false)
                }
                $0.borderColor(.clear)
                
            }
        }
    }
}

extension CreateTableViewCell: SendDataDelegate {
    func sendData() -> [(String, String)] {
        var returnArray = [(String, String)]()
        
        for index in textFieldsModels.indices {
            guard
                let textField = stackView.subviews[index] as? UITextField,
                let data = textField.text
            else { return [(String(), String())] }
            returnArray.append((textFieldsModels[index].textFieldId.rawValue, data))
        }
        return returnArray
    }
}
