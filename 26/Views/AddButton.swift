import UIKit

class AddButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        config()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func config() {
        setTitle(Constants.Titles.add, for: .normal)
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = #colorLiteral(red: 1, green: 0.4769005801, blue: 0.4608249571, alpha: 1)
        layer.cornerRadius = Constants.ButtonSetup.cornerRadius
        titleLabel?.numberOfLines = Constants.ButtonSetup.numberOfLines
        layer.shadowOpacity = Constants.ButtonSetup.shadowOpacity
        layer.shadowOffset = .init(
            width: Constants.ButtonSetup.shodowOffset,
            height: Constants.ButtonSetup.shodowOffset
        )
        contentEdgeInsets = .init(
            top: .zero,
            left: Constants.ButtonSetup.edgeXY,
            bottom: .zero,
            right: Constants.ButtonSetup.edgeXY
        )
    }
}
